package me.Jaryl.FoundBoxx.Threads;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import me.Jaryl.FoundBoxx.FoundBoxx;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;

public class Farmrate extends Thread {
	private FoundBoxx plugin;
	private ResultSet rs;
	private String name;
	private String days;
	private CommandSender asker;
	
    public Farmrate(FoundBoxx plugin, ResultSet rs, String name, String days, CommandSender asker) {
    	this.plugin = plugin;
    	this.rs = rs;
    	this.name = name;
    	this.days = days;
    	this.asker = asker;
    }
    
    public void run() {
		int coal = 0;
		int iron = 0;
		int red = 0;
		int lapis = 0;
		int gold = 0;
		int dias = 0;
		int emer = 0;
		HashMap<String, Integer> extra = new HashMap<String, Integer>();
		
		try {
			while (rs.next())
			{
				String id = rs.getString("block");
				if (id.equals("EMERALD_ORE"))
				{
					emer++;
				}
				if (id.equals("DIAMOND_ORE"))
				{
					dias++;
				}
				if (id.equals("GOLD_ORE"))
				{
					gold++;
				}
				if (id.equals("IRON_ORE"))
				{
					iron++;
				}
				if (id.equals("LAPIS_ORE"))
				{
					lapis++;
				}
				if (id.equals("REDSTONE_ORE") || id.equals("GLOWING_REDSTONE_ORE"))
				{
					red++;
				}
				if (id.equals("COAL_ORE"))
				{
					coal++;
				}
				
				if (plugin.ExtraBlks.size() > 0)
				{
					if (plugin.ExtraBlks.contains(id))
					{
						extra.put(id, (extra.containsKey(id) ? extra.get(id) + 1 : 0));
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		asker.sendMessage(ChatColor.AQUA + "[FoundBoxx] Farming rates for " + name + " for the past " + days + " day(s):");
		if (emer > 0)
			asker.sendMessage("    Emeralds: " + (dias > (50 * Integer.parseInt(days)) ? ChatColor.RED : (dias > (30 * Integer.parseInt(days)) ? ChatColor.YELLOW : "")) + emer);
		if (dias > 0)
			asker.sendMessage("    Diamonds: " + (dias > (70 * Integer.parseInt(days)) ? ChatColor.RED : (dias > (50 * Integer.parseInt(days)) ? ChatColor.YELLOW : "")) + dias);
		if (gold > 0)
			asker.sendMessage("    Gold: " + (gold > (170 * Integer.parseInt(days)) ? ChatColor.RED : (gold > (100 * Integer.parseInt(days)) ? ChatColor.YELLOW : "")) + gold);
		if (iron > 0)
			asker.sendMessage("    Iron: " + (iron > (500 * Integer.parseInt(days)) ? ChatColor.RED : (iron > (350 * Integer.parseInt(days)) ? ChatColor.YELLOW : "")) + iron);
		if (lapis > 0)
			asker.sendMessage("    Lapis Lazuli: " + (lapis > (90 * Integer.parseInt(days)) ? ChatColor.RED : (lapis > (60 * Integer.parseInt(days)) ? ChatColor.YELLOW : "")) + lapis);
		if (red > 0)
			asker.sendMessage("    Red Stone: " + red);
		if (coal > 0)
			asker.sendMessage("    Coal: " + coal);
		
		if (extra.size() > 0)
		{
			for (String b : extra.keySet())
			{
				asker.sendMessage("    '" + b + "': " + extra.get(b));
			}
		}
    }
}