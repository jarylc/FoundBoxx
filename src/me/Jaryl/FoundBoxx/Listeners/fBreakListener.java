package me.Jaryl.FoundBoxx.Listeners;

import me.Jaryl.FoundBoxx.FoundBoxx;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class fBreakListener implements Listener{
	private FoundBoxx plugin;
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockBreak(BlockBreakEvent event) {
		if (!event.isCancelled())
		{
			Block blk = event.getBlock();
			if (blk.hasMetadata("Found"))
				blk.removeMetadata("Found", this.plugin);
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockPlace(BlockPlaceEvent event) {
		if (!event.isCancelled() && event.getPlayer().getGameMode() != GameMode.CREATIVE)
		{
			Block blk = event.getBlock();
			if (plugin.canAnnounce(blk) != null)
			{
				blk.setMetadata("Found", new FixedMetadataValue(this.plugin, true));
				plugin.sql.queueData("INSERT INTO " + plugin.sqlPrefix + "_placed (x, y, z) VALUES (" + blk.getX() + ", " + blk.getY() + ", " + + blk.getZ() + ");");
			}
		}
	}
	
	public fBreakListener(FoundBoxx instance)
	{
		plugin = instance;
	}
}