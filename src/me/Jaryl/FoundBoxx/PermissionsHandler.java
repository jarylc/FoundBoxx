package me.Jaryl.FoundBoxx;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.tehkode.permissions.bukkit.PermissionsEx;
import ru.tehkode.permissions.PermissionUser;

public class PermissionsHandler {
	private FoundBoxx plugin; //NOTE TO SELF: RMB TO CHANGE THIS ON NEW PROJECTS
	private boolean PEX = false;

	public PermissionsHandler(FoundBoxx pl) { //NOTE TO SELF: RMB TO CHANGE THIS ON NEW PROJECTS
		plugin = pl;
	}

	public boolean hasPermission(Player p, String perm, boolean def, boolean ignoreop)
	{
		if (plugin.Perms)
		{
			if (PEX) {
				PermissionUser pp = PermissionsEx.getUser(p);
				return pp.has(perm);
			}
			return p.hasPermission(perm);
		}

		return ((!ignoreop && p.isOp()) || def);
	}
	public boolean hasPermission(CommandSender p, String perm, boolean def, boolean ignoreop)
	{
		if (!(p instanceof Player))
		{
			return true;
		}

		return hasPermission((Player)p, perm, def, ignoreop);
	}

	public void setupPermissions() {
		try {
			Class.forName("ru.tehkode.permissions.bukkit.PermissionsEx");

			PEX = true;
			System.out.println("[" + plugin.getDescription().getName() + "] PermissionsEX found for custom permissions plugin.");
		} catch( ClassNotFoundException e ) {
			System.out.println("[" + plugin.getDescription().getName() + "] No custom permission plugins found, using original permissions.");
		}
	}
}
